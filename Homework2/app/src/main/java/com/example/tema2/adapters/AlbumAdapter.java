package com.example.tema2.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tema2.interfaces.Links;
import com.example.tema2.models.Album;
import com.example.tema2.interfaces.OnItemClickListener;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import com.example.tema2.R;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.AlbumViewHolder> {

    ArrayList<Album> albums;
    OnItemClickListener onItemClickListener;
    Links links;
    public AlbumAdapter(ArrayList<Album> albums, OnItemClickListener onItemClickListener, Links links) {
        this.albums = albums;
        this.onItemClickListener = onItemClickListener;
        this.links = links;
    }
    @NonNull
    @Override
    public AlbumViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_album,parent, false);
        AlbumAdapter.AlbumViewHolder albumViewHolder = new AlbumAdapter.AlbumViewHolder(view);
        return albumViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumViewHolder holder, int position) {

        Album album = albums.get(position);
        holder.bind(album);
    }

    public int getItemCount() {
        return albums.size();
    }

    class AlbumViewHolder extends RecyclerView.ViewHolder {

        private View view;
        private TextView album;

        public AlbumViewHolder(View view) {
            super(view);
            this.album = view.findViewById(R.id.album);
            this.view = view;
        }

        public void bind(Album album) {

            this.album.setText(album.getName());
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onAlbumClick(album);
                    notifyItemChanged(getAdapterPosition());
                }
            });
        }

    }

}
