package com.example.tema2.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tema2.R;
import com.example.tema2.interfaces.Links;
import com.example.tema2.interfaces.OnItemClickListener;
import com.example.tema2.models.User;

import java.util.ArrayList;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {
    ArrayList<User> users;
    OnItemClickListener onItemClickListener;
    Links links;

    public UserAdapter(ArrayList<User> users, OnItemClickListener onItemClickListener, Links links) {
        this.users = users;
        this.onItemClickListener = onItemClickListener;
        this.links = links;
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        User user = users.get(position);
        holder.bind(user);
        boolean isExpanded = users.get(position).isExpanded();
        holder.expandableLayout.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_user, parent, false);
        UserViewHolder userViewHolder = new UserViewHolder(view);
        return userViewHolder;
    }

    class UserViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private View view;
        private ImageView arrow;
        private ConstraintLayout expandableLayout;
        private TextView posts;

        public UserViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            arrow = view.findViewById(R.id.arrow_button);
            posts = view.findViewById(R.id.post_description);
            this.view = view;
            expandableLayout = view.findViewById(R.id.expandable_window);
        }

        public void bind(User user) {
            name.setText(user.getName());
            String listOfPosts = new String();
            for (int index = 0; index < user.getPosts().size(); index++) {
                String post = new String();
                post += "* " + user.getPosts().get(index).getDescription() + "\n";
                listOfPosts += post;
            }
            posts.setText(listOfPosts);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onUserClick(user);
                    notifyItemChanged(getAdapterPosition());
                }
            });
            arrow.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    user.setExpanded(!user.isExpanded());
                    onItemClickListener.onArrowClick(user);
                    notifyItemChanged(getAdapterPosition());
                }
            });
        }
    }
}
